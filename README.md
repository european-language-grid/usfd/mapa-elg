# ELG wrapper for MAPA anonymisation service

This project is a thin wrapper for the [MAPA Project](https://gitlab.com/MAPA-EU-Project/mapa_project) anonymisation tool, to make it available as a web service that is compliant with the API specifications of the [European Language Grid](https://www.european-language-grid.eu).
