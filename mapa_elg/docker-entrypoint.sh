#!/bin/sh
exec /sbin/tini -- venv/bin/gunicorn --bind=0.0.0.0:$MAPA_PORT "--workers=$WORKERS" "--timeout=$TIMEOUT" "--worker-class=$WORKER_CLASS" --worker-tmp-dir=/dev/shm "$@" mapa_service:app
