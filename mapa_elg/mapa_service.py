from elg import FlaskService
from elg.model import TextsResponse, TextsResponseObject, Annotation

from collections import defaultdict

from mapa_project.webservice.loaded_components_managing import load_or_get_components_manager
from mapa_project.webservice.app.web_api_formats import AnonymizationOp

class MapaService(FlaskService):

    def mapa_to_elg(self, entities):
        elg_annots = defaultdict(list)
        for entity in entities:
            elg_annots[entity.entity_type].append(Annotation(start=entity.offset, end=(entity.offset + len(entity.entity_text))))
        return elg_annots

    def process_text(self, request):
        text = request.content
        operation = AnonymizationOp.NOOP
        try:
            operation = AnonymizationOp[request.params['operation']]
        except:
            # Either no params, or params.operation isn't a valid op
            pass

        anon_service = load_or_get_components_manager().get_anonymizer(load_or_get_components_manager().default_anonymizer_label)
        mapa_result = anon_service.anonymize(text=text, anonymization_op=operation)
        entities = sorted(mapa_result.level1_entities + mapa_result.level2_entities, key=lambda x: x.offset)
        return TextsResponse(texts=[
            TextsResponseObject(content=mapa_result.text, annotations=self.mapa_to_elg(entities))
        ])

# Preload components
load_or_get_components_manager()

service = MapaService("MAPA")
app = service.app
