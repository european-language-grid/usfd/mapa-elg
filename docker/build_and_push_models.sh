#!/bin/bash

set -xe

IMAGE_BASE="$1"
TAG="$2"

cd `dirname $0`

mkdir -p mapa_data

while read NAME URL ; do
  wget -O model.zip "$URL"
  mkdir tmp
  cd tmp
  unzip ../model.zip
  mv */* ../mapa_data/
  cd ..
  rm -rf tmp model.zip
  docker build --build-arg BASEIMG="$IMAGE_BASE/base:$TAG"  -t "$IMAGE_BASE/$NAME:$TAG" .
  [ -z "$NOPUSH" ] && docker push "$IMAGE_BASE/$NAME:$TAG"
  rm -rf mapa_data/*
done <<EOF
multilingual  https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/multilingual.zip
EOF
