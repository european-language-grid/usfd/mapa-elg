# This dockerfile builds an image that offers an ELG-compliant endpoint for a
# MAPA anonymisation service.  This image does not include a model, the model
# must be either bind-mounted in as /MODELS or added by building a
# model-specific child image - see docker/build_and_push_models.sh for details

FROM python:3.8-slim as build_mapa

COPY mapa_project /tmp/mapa_project
WORKDIR /tmp/mapa_project
RUN pip install --upgrade setuptools wheel && python setup.py bdist_wheel

FROM python:3.8-slim

# Install tini and create an unprivileged user
ADD https://github.com/krallin/tini/releases/download/v0.19.0/tini /sbin/tini
RUN addgroup --gid 1001 "elg" && adduser --disabled-password --gecos "ELG User,,," --home /elg --ingroup elg --uid 1001 elg && chmod +x /sbin/tini

# Copy in just the requirements file
COPY --chown=elg:elg mapa_elg/requirements.txt /elg/

# Everything from here down runs as the unprivileged user account
USER elg:elg

WORKDIR /elg

# Create a Python virtual environment for the dependencies
RUN python -m venv venv 
RUN /elg/venv/bin/python -m pip install --upgrade pip
RUN venv/bin/pip install --no-cache-dir torch==1.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
RUN venv/bin/pip --no-cache-dir install -r requirements.txt 
COPY --from=build_mapa --chown=elg:elg /tmp/mapa_project/dist /elg/
RUN venv/bin/pip --no-cache-dir install *.whl

COPY --chown=elg:elg docker/MODELS_README /MODELS/

ENV MAPA_BASE_FOLDER=/MODELS
ENV MAPA_PORT=8000
# Copy ini the entrypoint script and everything else our app needs

COPY --chown=elg:elg mapa_elg/docker-entrypoint.sh mapa_elg/mapa_service.py /elg/

ENV WORKERS=1
ENV TIMEOUT=30
ENV WORKER_CLASS=gthread
ENV LOGURU_LEVEL=INFO


RUN chmod +x ./docker-entrypoint.sh
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["--preload"]
